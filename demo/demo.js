// string number boolean ~ pass by value
// object , array ~ pass by reference
var isLogin = true;
console.log("isLogin", !isLogin);
// false
var num1 = 41;
var num2 = 4;
var ss1 = 12 >= 12;
var ss2 = num1 === num2;
var ss3 = num1 != num2;
console.log("ss2: ", ss2);

// console.log("ss1: ", ss1);

var ss4 = true && false && true;
var ss5 = true || false || false || false || false;
console.log("ss5: ", ss5);
console.log("ss4: ", ss4);
// ss &&  : chỉ đúng khi tất cả điều kiệm đúng
// ss || : chỉ sai khi tất cả điều kiện đều sai

var ss6 = 1 > 3 && 2 > 1;

// 1 true
// 0 false

// &&
// con có tiền, con ko có nhà, con đẹp trai

// ||
// con ko có tiền, ko có nhà, con làm biếng , nhưng mà con đẹp trai, con ở dơ
